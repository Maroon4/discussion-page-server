const Message = require('../models/message-model')

createMessage = (req, res) => {
    const body = req.body

    console.log(body)

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must create message',
        })
    }


    const message = new Message(body)

    if (!message) {
        return res.status(400).json({ success: false, error: err })
    }


    message
        .save()
        .then(() => {
            return res.status(201).json({
                success: true,
                id: message._id,
                message: 'message created!',
            })
        })
        .catch(error => {

            console.log(error)
            return res.status(400).json({
                error,

                message: 'message not created!',
            })
        })
}



updateMessage = async (req, res) => {
    const body = req.body

    console.log(body)
    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a body to update',
        })
    }

    Message.findOne({ _id: `${req.params.id}` }, (err, message) => {
        console.log({ _id: req.params.id })

        console.log(message)
        console.log(message._id)

        if (err) {
            return res.status(404).json({
                err,
                message: 'message not found!',
            })
        }

        message.name = body.name
        message.date = body.date
        message.text = body.text
        // message.comments = body.comments
        // message.iscomment = body.iscomment
        // message.parrentId = body.parrentId
        message
            .save()
            .then(() => {
                return res.status(200).json({
                    success: true,
                    id: message._id,
                    message: 'message updated!',
                })
            })
            .catch(error => {
                return res.status(404).json({
                    error,
                    message: 'message not updated!',
                })
            })
    })
}

deleteMessage = async (req, res) => {

    console.log(req.params.id)
    await Message.findOneAndDelete({ _id: req.params.id  } || {parentId: req.params.id}, (err, message) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!message) {
            return res
                .status(404)
                .json({ success: false, error: `message not found` })
        }

        return res.status(200).json({ success: true, data: message })
    }).catch(err => console.log(err))
}

getMessageById = async (req, res) => {
    await Message.findOne({ _id: req.params.id }, (err, message) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!message) {
            return res
                .status(404)
                .json({ success: false, error: `message not found` })
        }
        return res.status(200).json({ success: true, data: message })
    }).catch(err => console.log(err))
}

getMessages = async (req, res) => {
    await Message.find({}, (err, messages) => {
        // console.log(orders)

        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!messages.length) {
            return res
                .status(404)
                .json({ success: false, error: `message not found` })
        }
        return res.status(200).json({ success: true, data: messages })

    }).catch(err => console.log(err))
}


module.exports = {
    createMessage,
    updateMessage,
    deleteMessage,
    getMessages,
    getMessageById


}