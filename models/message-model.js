const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Message = new Schema(
    {
        name: { type: String, required: true },
        text: { type: String, required: true },
        date: { type: Date, required: true },
        // comments: { type: Array, required: true },
        iscomment: { type: Boolean, required: true },
        parentId: {type: String, required: true}

    },
    { timestamps: true },
);

module.exports = mongoose.model('message', Message);